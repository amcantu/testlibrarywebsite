﻿using TestLibraryWebSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestLibraryWebSite.DAL
{
    public class LibraryInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<LibraryContext>
    {
        protected override void Seed(LibraryContext context)
        {
            var authors = new List<Author>
            {
            new Author{FirstName="Mark",LastName="Lemmon",BirthDay=DateTime.Parse("2005-09-01")},
            new Author{FirstName="Krister",LastName="Ahlersten",BirthDay=DateTime.Parse("2002-09-01")},
            new Author{FirstName="Miguel",LastName="de Cervantes",BirthDay=DateTime.Parse("2003-09-01")},
            new Author{FirstName="Benjamin",LastName="Graham",BirthDay=DateTime.Parse("2002-09-01")},
            };
            authors.ForEach(s => context.Authors.Add(s));
            context.SaveChanges();

            var categories = new List<Category>
            {
            new Category{Description="Math"},
            new Category{Description="Literature"},
            new Category{Description="Economy"},
            };
            categories.ForEach(s => context.Categories.Add(s));
            context.SaveChanges();

            var books = new List<Book>
            {
            new Book{AuthorId=1,CategoryId=1,Title="Mathematics for Computer Scientists",PublicationYear=2000},
            new Book{AuthorId=2,CategoryId=1,Title="Introduction to MatLab",PublicationYear=1990},
            new Book{AuthorId=3,CategoryId=2,Title="Don Quijote de la Mancha",PublicationYear=2014},
            new Book{AuthorId=3,CategoryId=2,Title="La Galetea",PublicationYear=2013},
            new Book{AuthorId=4,CategoryId=3,Title="The Intelligent Investor",PublicationYear=2008},
            };
            books.ForEach(s => context.Books.Add(s));
            context.SaveChanges();
        }
    }
}