﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using TestLibraryWebSite.DAL;
using TestLibraryWebSite.Models;

namespace TestLibraryWebSite.Controllers
{
    public class BooksController : ApiController
    {
        private LibraryContext db = new LibraryContext();

        // GET: api/Books
        public IQueryable<Book> GetBooks()
        {
            return db.Books.Include("Category").Include("Author");
        }

        // GET: api/Books?AuthorId=1
        public IQueryable<Book> GetBooksByAuthorId(int AuthorId)
        {
            return from b in db.Books.Include("Category").Include("Author")
                   where b.AuthorId == AuthorId
                   select b;

        }

        // GET: api/Books?CategoryId=1
        public IQueryable<Book> GetBooksByCategoryId(int CategoryId)
        {
            return from b in db.Books.Include("Category").Include("Author")
                   where b.CategoryId == CategoryId
                   select b;

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}