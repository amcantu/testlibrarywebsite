﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestLibraryWebSite.DAL;

namespace TestLibraryWebSite.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            LibraryContext db = new LibraryContext();
            ViewBag.Authors = db.Authors;

            return View();
        }

    }
}